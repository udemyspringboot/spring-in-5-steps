package com.in28minutes.spring.basics.springin5steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.in28minutes.spring.basics.springin5steps.xml.XmlPersonDAO;

public class SpringIn5StepsXmlApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpringIn5StepsXmlApplication.class);
	public static void main(String[] args) {
		try (final ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml")) {
			LOGGER.info("Beans Loaded -> {}", (Object) applicationContext.getBeanDefinitionNames());
			final XmlPersonDAO xmlPersonImpl = applicationContext.getBean(XmlPersonDAO.class);
			System.out.println(xmlPersonImpl);
			System.out.println(xmlPersonImpl.getXmlJdbcConnection());
		}
	}

}
