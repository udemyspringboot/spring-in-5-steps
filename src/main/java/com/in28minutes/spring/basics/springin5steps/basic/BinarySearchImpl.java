package com.in28minutes.spring.basics.springin5steps.basic;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class BinarySearchImpl {

	private static final Logger LOGGER = LoggerFactory.getLogger(BinarySearchImpl.class);

	@Autowired
	@Qualifier("quick")
	private SortAlgorithm sortAlgorithm;

	public int binarySearch(final int[] numbers, final int numberToSearch) {
		final int[] sortedNumbers = this.sortAlgorithm.sort(numbers);
		System.out.println(this.sortAlgorithm.getClass().getName());
		//search the array
		return 3;
	}

	@PostConstruct
	public void postConstruct() {
		LOGGER.info("postConstruct");
	}

	@PreDestroy
	public void preDestroy() {
		LOGGER.info("preDestroy");
	}
}
