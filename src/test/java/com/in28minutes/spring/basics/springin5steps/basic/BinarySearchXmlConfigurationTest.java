package com.in28minutes.spring.basics.springin5steps.basic;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.in28minutes.spring.basics.springin5steps.SpringIn5StepsBasicApplication;

// load the context
@RunWith(SpringRunner.class)
@ContextConfiguration(locations = "/testContext.xml")
public class BinarySearchXmlConfigurationTest {

	// get this bean from the context
	@Autowired
	private BinarySearchImpl binarySearchTest;

	@Test
	public void testBasicScenario() {
		// call methdo on binarySearch
		final int actualResult = this.binarySearchTest.binarySearch(new int[] {}, 5);
		// check if the value is correct
		assertEquals(3, actualResult);
	}

}
