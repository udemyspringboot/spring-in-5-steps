package com.in28minutes.spring.basics.springin5steps.cdi;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import com.in28minutes.spring.basics.springin5steps.SpringIn5StepsBasicApplication;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = SpringIn5StepsBasicApplication.class)
public class SomeCdiBusinessTest {

	// Inject Mock
	@InjectMocks
	private SomeCdiBusiness someCdiBusiness;

	// Create Mock
	@Mock
	private SomeCdiDao someCdiDao;

	@Test
	public void testBasicScenario() {
		Mockito.when(this.someCdiDao.getData()).thenReturn(new int[] { 2, 4 });
		assertEquals(4, this.someCdiBusiness.findGreatest());
	}

	@Test
	public void testBasicScenario_NoElements() {
		Mockito.when(this.someCdiDao.getData()).thenReturn(new int[] {});
		assertEquals(Integer.MIN_VALUE, this.someCdiBusiness.findGreatest());
	}

	@Test
	public void testBasicScenario_EqualElements() {
		Mockito.when(this.someCdiDao.getData()).thenReturn(new int[] { 2, 2 });
		assertEquals(2, this.someCdiBusiness.findGreatest());
	}
}
